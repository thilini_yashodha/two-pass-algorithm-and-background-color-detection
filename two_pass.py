from PIL import Image
from IPython.display import display
import math, random
from itertools import product
import numpy as np
from collections import Counter


#class to detect the background colour of the given image input
class BackgroundColorDetector():
    def __init__(self,image):
        self.img = np.array(image)
        self.img=self.img.astype(int)
        print(self.img)
        self.manual_count = {}         #store frequecy of colour (while & black)
        self.w, self.h = self.img.shape  #get image width & height
        self.total_pixels = self.w*self.h  #get total number of pixels in the image

    def count(self):
        for y in range(0, self.h):
            for x in range(0, self.w):
                colour =  self.img[x, y]
                if colour in self.manual_count:
                    self.manual_count[colour] += 1
                else:
                    self.manual_count[colour] = 1

    def average_colour(self):
        colour = 0
        sample = 2   #for 2 colours in binary image (white & black)
        for top in range(0, sample):
            colour += self.number_counter[top][0][0]

        # average_colour = colour / sample

    def twenty_most_common(self):
        self.count()
        self.number_counter = Counter(self.manual_count).most_common(20)  #getting number of pixels having relavent colour

    def detect(self):
        self.twenty_most_common()
        #getting the persentage of each colour
        self.percentage_of_first = (
            float(self.number_counter[0][1])/self.total_pixels)
        if self.percentage_of_first > 0.5:
            return self.number_counter[0][0]  #return the most common colour (0 for black &  1 for white)
        # else:
        #     average_colour()


class unionFind:
    def __init__(self):
        #array to hold labels 
        self.P = []

        # name of the label
        self.label = 0

    def makeLabel(self):
        r = self.label
        self.label += 1
        self.P.append(r)
        return r
    
    # Makes all nodes "in the path of node i" point to root
    def setRoot(self, i, root):
        while self.P[i] < i:
            j = self.P[i]
            self.P[i] = root
            i = j
        self.P[i] = root

    # finds the root node of the tree containing node i
    def findRoot(self, i):
        while self.P[i] < i:
            i = self.P[i]
        return i
    
    # finds the root of the tree containing node i while compresses the tree
    def find(self, i):
        root = self.findRoot(i)
        self.setRoot(i, root)
        return root
    
    # joining the two trees containing nodes i and j
    def union(self, i, j):
        if i != j:
            root = self.findRoot(i)
            rootj = self.findRoot(j)
            if root > rootj: root = rootj
            self.setRoot(j, root)
            self.setRoot(i, root)
    
    def flatten(self):
        for i in range(1, len(self.P)):
            self.P[i] = self.P[self.P[i]]
    
    def flattenL(self):
        k = 1
        for i in range(1, len(self.P)):
            if self.P[i] < i:
                self.P[i] = self.P[self.P[i]]
            else:
                self.P[i] = k
                k += 1

'''two pass algorithm'''

def twoPass(img):

    #getting image
    data = img.load()
    
    width, height = img.size  #getting image width & height
 
    # union find data structure
    uf = unionFind()
 
    '''first pass'''
 
    # Dictionary of point:label pairs
    labels = {}
 
    for y, x in product(range(height), range(width)):
 
        '''8 connectivity is considered to get neighbours
        If pixel are named as:
        
          -------------
          | u | v | w |
          -------------
          | z | t |   |
          -------------
          |   |   |   |
          -------------
        
        The current pixel is t
        u, v, w, and z are its neighbors of interest by considering 8 connectivity
        
        255 is white, 0 is blackwhite. pixels are background'''
        if data[x, y] == 255:
            pass
 

        #If u,w & y are neighbours of b then v's value assign to t
        elif y > 0 and data[x, y-1] == 0:
            labels[x, y] = labels[(x, y-1)]
 

        #If v is neighbour of w but u & z are not then have to check u & z labels
        elif x+1 < width and y > 0 and data[x+1, y-1] == 0:
 
            c = labels[(x+1, y-1)]
            labels[x, y] = c

            #If u & w are connected through t then union theri sets
            if x > 0 and data[x-1, y-1] == 0:
                a = labels[(x-1, y-1)]
                uf.union(c, a)

            #If x & w are connected through t then union their sets
            elif x > 0 and data[x-1, y] == 0:
                d = labels[(x-1, y)]
                uf.union(c, d)

        #If u belongs to one label, v & w are belong to background then z is a neighbour of u. So they have same label
        #So u's label is assigned to t
        elif x > 0 and y > 0 and data[x-1, y-1] == 0:
            labels[x, y] = labels[(x-1, y-1)]
 

        #If u,v & w are in background then d's label is assigned to t
        elif x > 0 and data[x-1, y] == 0:
            labels[x, y] = labels[(x-1, y)]
 
        #If all neighbours aee white then they are in background. Then the current pixel is a new component
        else: 
            labels[x, y] = uf.makeLabel()

    '''second pass'''
 
    uf.flatten()
 
    colors = {}

    #creating RGB image to display different connected components
    output = Image.new("RGB", (width, height))
    outdata = output.load()

    for (x, y) in labels:
 
        #current pixel label
        component = uf.find(labels[(x, y)])

        # update the labels with correct information
        labels[(x, y)] = component
 
        # Assign a random color with this component 
        if component not in colors: 
            colors[component] = (random.randint(0,255), random.randint(0,255),random.randint(0,255))

        # Colorize the image
        outdata[x, y] = colors[component]

    return output


''''main'''

#give image path here
path='input5.bmp'


#read image using pillow image
img = Image.open(path)
#output given image
img.show()

#convert image into a binary image using a threshold value
img = img.point(lambda p: p > 190 and 255)
img = img.convert('1')

'''find the background colour of the given image input'''
#creat an object of BackgroundColourDetector class
BackgroundColor = BackgroundColorDetector(img)
#detecting colour. return 0 for black & 1 for white
background_colour=BackgroundColor.detect()

'''convert image background colour to white and component colour to black if current background colour is black'''
if int(background_colour) == 0:
    #converte image into integer numpy array
    img2=np.asarray(img)
    img2=img2.astype(int)

    #convert colour
    sub=1-img2
    sub=sub.astype(bool)

    #convert array into pillow image
    img=Image.fromarray(sub)

'''call two pass algorithm'''
output = twoPass(img)
output.show()
output.save(path+'_output.jpg')